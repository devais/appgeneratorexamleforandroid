package com.pestudio;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.xml.sax.Parser;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public int rez;
    public int answer;
    public int countTrue = 0;
    public int countErr = 0;
    public int countTotal = 0;

    TextView txtYes;
    TextView txtNo;
    TextView txtAllExmpl;
    TextView txtNum1;
    TextView txtNum;
    TextView txtOper;

    Button btnAnswer;

    EditText answerTxt;
    numRandom random = new numRandom();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtYes = (TextView) findViewById(R.id.txtYes);
        txtNo = (TextView) findViewById(R.id.txtNo);
        txtAllExmpl = (TextView) findViewById(R.id.txtAllExmpl);
        txtNum = (TextView) findViewById(R.id.txtNum);
        txtNum1 = (TextView) findViewById(R.id.txtNum1);
        txtOper = (TextView) findViewById(R.id.txtOper);

        Button btnAnswer = (Button) findViewById(R.id.btnAnswer);
        btnAnswer.setOnClickListener(this);

        EditText answerTxt = (EditText) findViewById(R.id.answerTxt);

    }
    @Override
    public void onClick(View v) {
        txtYes.setText("Верно: " + countTrue);
        countTrue++;

    }
    void getOperation() {
        String chars = "+-/*";
        Random rnd = new Random();
        char type = chars.charAt(rnd.nextInt(chars.length()));
        switch (type) {
            case '+':
                addition();
                break;
            case '-':
                deduction();
                break;
            case '*':
                multiplication();
                break;
            case '/':
                division();
                break;
            }
    }
    public void addition(){
        int num = random.randNum100();
        int num1 = random.randNum100();
        answer = Integer.parseInt(answerTxt.getText().toString());
        txtNum.setText(num);
        txtNum1.setText(num1);
        txtOper.setText("+");
        rez = num + num1;
        setCounter();
        setRezult();
        }

    void deduction(){

    }

    void multiplication(){
        int num = random.randNum100();
        int num1 = random.randNum100();
        answer = Integer.parseInt(answerTxt.getText().toString());
        txtNum.setText(num);
        txtNum1.setText(num1);
        txtOper.setText("*");
        rez = num * num1;
        setCounter();
        setRezult();

    }

    void division(){

    }
    void setCounter(){
    if (rez == answer) {
        countTrue++;
    } else {
        countErr++;
    }
    }
    void setRezult(){
        txtYes.setText("Верно: " + countTrue);
        txtNo.setText("Неверно: " + countErr);
        txtAllExmpl.setText("Всего: " + countTotal);

    }


}